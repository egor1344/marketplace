create table products
(
    id    serial not null
        constraint products_pk
            primary key,
    name  text,
    price integer,
    image text
);

alter table products
    owner to postgres;

create unique index products_id_uindex
    on products (id);

create table users
(
    id       serial not null
        constraint users_pk
            primary key,
    fio      text default ''::text,
    email    text,
    phone    text default ''::text,
    password text   not null
);

alter table users
    owner to postgres;

create unique index users_email_uindex
    on users (email);

create unique index users_id_uindex
    on users (id);

create table users_card
(
    user_id integer
        constraint users_card_users_id_fk
            references users
            on delete cascade,
    id      serial not null
        constraint users_card_pk
            primary key,
    date    date    default CURRENT_DATE,
    booked  boolean default false
);

alter table users_card
    owner to postgres;

create unique index users_card_id_uindex
    on users_card (id);

create table users_card_products
(
    users_card          integer
        constraint users_card_products_users_card_id_fk
            references users_card
            on delete cascade,
    product             integer
        constraint users_card_products_products_id_fk
            references products
            on delete set null,
    notify_change_price boolean default false
);

alter table users_card_products
    owner to postgres;

create unique index users_card_products_users_card_product_uindex
    on users_card_products (users_card, product);

