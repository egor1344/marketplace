package notify_api

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"marketplace/internal/api"
	"marketplace/internal/domain/services"
	log "marketplace/pkg/logger"
)

var amqpDsn string
var amqpEmailQueueName string
var amqpPushQueueName string

// initNotifyRestServer - инициализация rest сервера
func initNotifyRestServer() (*api.NotifyServer, error) {
	log.Logger.Info("init notify rest service")
	queueMap := make(map[string]string)
	queueMap["email"] = amqpEmailQueueName
	queueMap["push"] = amqpPushQueueName
	notify := services.NotifyService{AmqpDSN: amqpDsn, Log: log.Logger, NotifyMap: queueMap}
	return &api.NotifyServer{Log: log.Logger, Notify: notify}, nil
}

// RunEmailRestServer - инициализация rest сервера
func RunEmailRestServer(c chan bool) {
	log.Logger.Info("run rest email server")
	restServer, err := initNotifyRestServer()
	if restServer == nil {
		c <- true
		log.Logger.Fatal("Error run server")
		return
	}
	if err != nil {
		log.Logger.Error("error run rest server ", err)
	}
	address := viper.GetString("API_REST_HOST") + ":" + viper.GetString("API_NOTIFY_REST_PORT")
	log.Logger.Info(address)
	restServer.RunServer(address)
	c <- true
}

// Notify cobra run server
var NotifyCmd = &cobra.Command{
	Use:   "notify_api",
	Short: "run notify api",
	Run: func(cmd *cobra.Command, args []string) {
		c := make(chan bool)
		RunEmailRestServer(c)
	},
}

func init() {
	log.Logger.Info("Init notify api service")
	err := viper.BindEnv("QUEUE_NAME_EMAIL_NOTIFY")
	if err != nil {
		log.Logger.Info(err)
	}
	err = viper.BindEnv("QUEUE_NAME_PUSH_NOTIFY")
	if err != nil {
		log.Logger.Info(err)
	}
	err = viper.BindEnv("AMQP_DSN")
	if err != nil {
		log.Logger.Info(err)
	}
	viper.AutomaticEnv()
	log.Logger.Info(viper.AllSettings())
	amqpDsn = viper.GetString("AMQP_DSN")
	amqpEmailQueueName = viper.GetString("QUEUE_NAME_EMAIL_NOTIFY")
	amqpPushQueueName = viper.GetString("QUEUE_NAME_PUSH_NOTIFY")
}
