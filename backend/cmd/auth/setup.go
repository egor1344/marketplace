package auth

import (
	"encoding/base64"
	"github.com/volatiletech/authboss-clientstate"
	_ "github.com/volatiletech/authboss/auth"
	_ "github.com/volatiletech/authboss/logout"
	_ "github.com/volatiletech/authboss/recover"
	_ "github.com/volatiletech/authboss/register"
	"os"
)

func CustomSessionCookieName() *abclientstate.SessionStorer {
	var ss abclientstate.SessionStorer
	sessionStoreKey, _ := base64.StdEncoding.DecodeString(os.Getenv("SESSION_KEY"))
	ss = abclientstate.NewSessionStorer(os.Getenv("SESSION_COOKIE_NAME"), sessionStoreKey, nil)

	return &ss
}
