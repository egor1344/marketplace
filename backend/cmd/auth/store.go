package auth

import (
	"context"
	"github.com/spf13/viper"
	"github.com/volatiletech/authboss"
	"marketplace/internal/database/postgres"
	"marketplace/internal/domain/interfaces/databases"
	"marketplace/internal/domain/models/user"
	log "marketplace/pkg/logger"
)

var (
	assertUser   = &user.User{}
	assertStorer = &MemStorer{}

	_ authboss.User         = assertUser
	_ authboss.AuthableUser = assertUser

	_ authboss.CreatingServerStorer = assertStorer
)

// MemStorer stores users in memory
type MemStorer struct {
	DBStorage databases.AuthDatabase // Интерфейс для работы с БД
}

// NewMemStorer constructor
func NewMemStorer() *MemStorer {
	viper.AutomaticEnv()
	dbDsn := viper.GetString("DB_DSN")
	if dbDsn == "" {
		log.Logger.Error("dont set env variable DB_DSN")
	}
	database, err := postgres.InitPgBannerStorage(dbDsn)
	if err != nil {
		log.Logger.Error("databases error ", err)
	}
	database.Log, err = log.GetLogger()
	if err != nil {
		log.Logger.Error("get logger error")
	}
	return &MemStorer{
		DBStorage: database,
	}
}

// Save the user
func (m MemStorer) Save(_ context.Context, user authboss.User) error {
	println("Saved user:", user.GetPID())
	return nil
}

// Load the user
func (m MemStorer) Load(ctx context.Context, email string) (user authboss.User, err error) {
	u, err := m.DBStorage.LoadUser(ctx, email)
	if err != nil {
		return nil, authboss.ErrUserNotFound
	}
	return &u, nil
}

// New user creation
func (m MemStorer) New(_ context.Context) authboss.User {
	return &user.User{}
}

// Create the user
func (m MemStorer) Create(ctx context.Context, newUser authboss.User) error {
	u := newUser.(*user.User)
	err := m.DBStorage.CreateUser(ctx, *u)
	if err != nil {
		return authboss.ErrUserFound
	}

	println("Created new user:", newUser.GetPID())
	return nil
}
