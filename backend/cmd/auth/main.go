package auth

import (
	"context"
	"encoding/base64"
	"flag"
	"github.com/spf13/cobra"
	"log"
	"marketplace/internal/domain/models/user"
	"net/http"
	"os"
	"regexp"
	"time"

	"github.com/volatiletech/authboss"
	"github.com/volatiletech/authboss-clientstate"
	"github.com/volatiletech/authboss-renderer"
	_ "github.com/volatiletech/authboss/auth"
	"github.com/volatiletech/authboss/defaults"
	_ "github.com/volatiletech/authboss/logout"
	_ "github.com/volatiletech/authboss/recover"
	_ "github.com/volatiletech/authboss/register"

	"github.com/go-chi/chi"
	"github.com/gorilla/schema"
	"github.com/gorilla/sessions"
	"github.com/justinas/nosurf"
)

var (
	ab        = authboss.New()
	database  = NewMemStorer()
	schemaDec = schema.NewDecoder()

	sessionStore      abclientstate.SessionStorer
	cookieStore       abclientstate.CookieStorer
	sessionCookieName = os.Getenv("SESSION_COOKIE_NAME")
)

func setupAuthboss() {
	ab.Config.Paths.RootURL = "https://localhost"

	ab.Config.Storage.Server = database
	ab.Config.Storage.SessionState = sessionStore
	ab.Config.Storage.CookieState = cookieStore
	ab.Config.Core.ViewRenderer = defaults.JSONRenderer{}
	ab.Config.Core.MailRenderer = abrenderer.NewEmail("/auth", "ab_views")
	ab.Config.Modules.RegisterPreserveFields = []string{"email", "name"}

	defaults.SetCore(&ab.Config, true, false)
	emailRule := defaults.Rules{
		FieldName: "email", Required: true,
		MatchError: "Must be a valid e-mail address",
		MustMatch:  regexp.MustCompile(`.*@.*\.[a-z]+`),
	}
	passwordRule := defaults.Rules{
		FieldName: "password", Required: true,
		MinLength: 4,
	}
	nameRule := defaults.Rules{
		FieldName: "name", Required: true,
		MinLength: 2,
	}

	ab.Config.Core.BodyReader = defaults.HTTPBodyReader{
		ReadJSON: true,
		Rulesets: map[string][]defaults.Rules{
			"register":    {emailRule, passwordRule, nameRule},
			"recover_end": {passwordRule},
		},
		Confirms: map[string][]string{
			"register":    {"password", authboss.ConfirmPrefix + "password"},
			"recover_end": {"password", authboss.ConfirmPrefix + "password"},
		},
		Whitelist: map[string][]string{
			"register": {"email", "name", "password"},
		},
	}
	// Initialize authboss (instantiate modules etc.)
	if err := ab.Init(); err != nil {
		panic(err)
	}
}

func RunAuthServer(c chan bool) {
	flag.Parse()
	cookieStoreKey, _ := base64.StdEncoding.DecodeString(os.Getenv("COOKIES_KEY"))
	sessionStoreKey, _ := base64.StdEncoding.DecodeString(os.Getenv("SESSION_KEY"))
	cookieStore = abclientstate.NewCookieStorer(cookieStoreKey, nil)
	cookieStore.HTTPOnly = false
	cookieStore.Secure = false
	sessionStore = abclientstate.NewSessionStorer(sessionCookieName, sessionStoreKey, nil)
	sessionStore := sessionStore.Store.(*sessions.CookieStore)
	sessionStore.Options.HttpOnly = false
	sessionStore.Options.Secure = false
	sessionStore.MaxAge(int((30 * 24 * time.Hour) / time.Second))

	setupAuthboss()

	schemaDec.IgnoreUnknownKeys(true)

	mux := chi.NewRouter()
	mux.Use(logger, ab.LoadClientStateMiddleware, dataInjector)

	// Routes
	mux.Group(func(mux chi.Router) {
		mux.Use(authboss.ModuleListMiddleware(ab))
		mux.Mount("/auth", http.StripPrefix("/auth", ab.Config.Core.Router))
	})

	optionsHandler := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-CSRF-TOKEN", nosurf.Token(r))
		w.WriteHeader(http.StatusOK)
	}
	routes := []string{"login", "logout", "recover", "recover/end", "register"}
	mux.MethodFunc("OPTIONS", "/*", optionsHandler)
	for _, r := range routes {
		mux.MethodFunc("OPTIONS", "/auth/"+r, optionsHandler)
	}

	// Start the server
	port := os.Getenv("API_AUTH_PORT")
	if len(port) == 0 {
		port = "3000"
	}
	log.Printf("Listening on 0.0.0.0:%s", port)
	log.Println(http.ListenAndServe("0.0.0.0:"+port, mux))
	c <- true
}

// AuthCmd cobra run server
var AuthCmd = &cobra.Command{
	Use:   "auth",
	Short: "run auth service",
	Run: func(cmd *cobra.Command, args []string) {
		c := make(chan bool)
		RunAuthServer(c)
	},
}

func dataInjector(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		data := layoutData(w, &r)
		r = r.WithContext(context.WithValue(r.Context(), authboss.CTXKeyData, data))
		handler.ServeHTTP(w, r)
	})
}

func layoutData(w http.ResponseWriter, r **http.Request) authboss.HTMLData {
	currentUserName := ""
	userInter, err := ab.LoadCurrentUser(r)
	if userInter != nil && err == nil {
		currentUserName = userInter.(*user.User).Email
	}

	return authboss.HTMLData{
		"loggedin":          userInter != nil,
		"current_user_name": currentUserName,
		"csrf_token":        nosurf.Token(*r),
		"flash_success":     authboss.FlashSuccess(w, *r),
		"flash_error":       authboss.FlashError(w, *r),
	}
}
