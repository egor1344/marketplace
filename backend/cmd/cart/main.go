package cart

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	abclientstate "github.com/volatiletech/authboss-clientstate"
	"marketplace/cmd/auth"
	"marketplace/internal/api"
	"marketplace/internal/database/postgres"
	"marketplace/internal/domain/services"
	log "marketplace/pkg/logger"
	"os"
)

var SessionStore *abclientstate.SessionStorer
var SessionName = os.Getenv("SESSION_COOKIE_NAME")

// initRestServer - инициализация rest сервера
func initRestServer() (*api.CartServer, error) {
	dbDsn := viper.GetString("DB_DSN")
	if dbDsn == "" {
		log.Logger.Error("dont set env variable DB_DSN")
	}
	database, err := postgres.InitPgBannerStorage(dbDsn)
	if err != nil {
		log.Logger.Error("databases error ", err)
	}
	database.Log, err = log.GetLogger()
	if err != nil {
		log.Logger.Error("get logger error")
	}
	apiService := services.CartService{Database: database, Log: log.Logger}
	amqpDsn := viper.GetString("AMQP_DSN")
	if amqpDsn == "" {
		log.Logger.Error("dont set env variable AMQP_DSN")
	}
	queueMap := make(map[string]string)
	queueMap["email"] = viper.GetString("QUEUE_NAME_EMAIL_NOTIFY")
	queueMap["push"] = viper.GetString("QUEUE_NAME_PUSH_NOTIFY")
	notify := services.NotifyService{AmqpDSN: amqpDsn, Log: log.Logger, NotifyMap: queueMap}
	SessionStore = auth.CustomSessionCookieName()
	return &api.CartServer{CartService: &apiService, Log: log.Logger, Notify: notify, Session: SessionStore}, nil
}

// RunCartRestServer - инициализация rest сервера
func RunCartRestServer(c chan bool) {
	log.Logger.Info("run rest cart_api server")
	restServer, err := initRestServer()
	if restServer == nil {
		c <- true
		log.Logger.Fatal("Error run server")
		return
	}
	if err != nil {
		log.Logger.Error("error run rest server ", err)
	}
	address := viper.GetString("API_REST_HOST") + ":" + viper.GetString("API_CART_PORT")
	log.Logger.Info(address)
	restServer.RunServer(address)
	c <- true
}

// CartCmd - Комманда для cobra
var CartCmd = &cobra.Command{
	Use:   "cart",
	Short: "run cart api server",
	Run: func(cmd *cobra.Command, args []string) {
		c := make(chan bool)
		RunCartRestServer(c)
	},
}

func init() {
	err := viper.BindEnv("DB_DSN")
	err = viper.BindEnv("API_CART_PORT")
	err = viper.BindEnv("AMQP_DSN")
	err = viper.BindEnv("QUEUE_NAME_EMAIL_NOTIFY")
	err = viper.BindEnv("QUEUE_NAME_PUSH_NOTIFY")
	if err != nil {
		log.Logger.Info(err)
	}
	viper.AutomaticEnv()
	log.Logger.Info(viper.AllSettings())
}
