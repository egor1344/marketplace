package cmd

import (
	"github.com/spf13/cobra"
	"marketplace/cmd/auth"
	"marketplace/cmd/cart"
	"marketplace/cmd/email_notify"
	"marketplace/cmd/notify_api"
	"marketplace/cmd/product_api"
	"marketplace/cmd/push_notify"
)

// RootCmd - cobra command
var RootCmd = &cobra.Command{
	Use:   "product",
	Short: "product services",
	Run: func(cmd *cobra.Command, args []string) {
		rest := make(chan bool)
		go product_api.RunProductRestServer(rest)
		for {
			_, ok1 := <-rest
			if !ok1 {
				break
			}
		}
	},
}

func init() {
	RootCmd.AddCommand(product_api.RestAPIServerCmd)
	RootCmd.AddCommand(email_notify.MailingCmd)
	RootCmd.AddCommand(push_notify.PushingCmd)
	RootCmd.AddCommand(notify_api.NotifyCmd)
	RootCmd.AddCommand(auth.AuthCmd)
	RootCmd.AddCommand(cart.CartCmd)
}
