package push_notify

import (
	_ "github.com/jackc/pgx/stdlib"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	log "marketplace/pkg/logger"
)

var amqpDsn string
var amqpQueueName string

//var pushingDBDSN string

type pushNotify struct {
	amqpDsn, amqpQueueName string
}

// RunServerPushlNotify - создание подключения к необходимым приложениям и запуск сервиса
func (rbs *pushNotify) RunServerPushNotify() {

	// RabbitMQ
	conn, err := amqp.Dial(rbs.amqpDsn)
	if err != nil {
		log.Logger.Info(rbs.amqpDsn)
		log.Logger.Fatal(err)
	}
	defer conn.Close()
	rmqCh, err := conn.Channel()
	if err != nil {
		log.Logger.Fatal(err)
	}
	defer rmqCh.Close()

	// Подключение к очереди
	queue, err := rmqCh.QueueDeclare(
		rbs.amqpQueueName,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Logger.Fatal(err)
	}

	rbs.runPushing(rmqCh, queue)

}

// runPushing - Запуск сервиса
func (rbs *pushNotify) runPushing(rmqCh *amqp.Channel, queue amqp.Queue) {
	messages, err := rmqCh.Consume(
		queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Logger.Fatal(err)
	}
	forever := make(chan bool)
	// Получение сообщений из очереди
	go func() {
		for d := range messages {
			// Эмулирование работы отправки сообщения
			log.Logger.Info("Received a message: %s", d.Body)
		}
	}()
	log.Logger.Info("Сервис по рассылке push уведомлений успешно запущен")
	<-forever
}

// Pushing cobra run server
var PushingCmd = &cobra.Command{
	Use:   "push_notify",
	Short: "run push_notify",
	Run: func(cmd *cobra.Command, args []string) {
		n := pushNotify{amqpDsn, amqpQueueName}
		n.RunServerPushNotify()
	},
}

func init() {
	log.Logger.Info("Init push service")
	err := viper.BindEnv("QUEUE_NAME_EMAIL_NOTIFY")
	if err != nil {
		log.Logger.Info(err)
	}
	err = viper.BindEnv("AMQP_DSN")
	if err != nil {
		log.Logger.Info(err)
	}
	//err = viper.BindEnv("DB_DSN")
	//if err != nil {
	//	log.Logger.Info(err)
	//}
	viper.AutomaticEnv()
	log.Logger.Info(viper.AllSettings())
	amqpDsn = viper.GetString("AMQP_DSN")
	//pushingDBDSN = viper.GetString("db_dsn")
	amqpQueueName = viper.GetString("QUEUE_NAME_PUSH_NOTIFY")
}
