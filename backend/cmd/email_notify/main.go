package email_notify

import (
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/streadway/amqp"
	log "marketplace/pkg/logger"
)

var amqpDsn string
var amqpQueueName string
var mailingDBDSN string

type emailNotify struct {
	amqpDsn, amqpQueueName, mailingDBDSN string
}

// RunServerEmailNotify - создание подключения к необходимым приложениям и запуск сервиса
func (rbs *emailNotify) RunServerEmailNotify(db_dsn, amqp_dsn, amqp_queue_name string) {

	// PostgreSQL
	log.Logger.Info(db_dsn)
	pgSQL, err := sqlx.Connect("pgx", db_dsn)
	if err != nil {
		log.Logger.Fatal(err)
	}
	defer pgSQL.Close()

	// RabbitMQ
	conn, err := amqp.Dial(amqp_dsn)
	if err != nil {
		log.Logger.Fatal(err)
	}
	defer conn.Close()
	rmqCh, err := conn.Channel()
	if err != nil {
		log.Logger.Fatal(err)
	}
	defer rmqCh.Close()

	// Подключение к очереди
	queue, err := rmqCh.QueueDeclare(
		amqp_queue_name,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Logger.Fatal(err)
	}

	rbs.runMailing(rmqCh, queue)

}

// runMailing - Запуск сервиса
func (rbs *emailNotify) runMailing(rmqCh *amqp.Channel, queue amqp.Queue) {
	messages, err := rmqCh.Consume(
		queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Logger.Fatal(err)
	}
	forever := make(chan bool)
	// Получение сообщений из очереди
	go func() {
		for d := range messages {
			// Эмулирование работы отправки сообщения
			log.Logger.Info("Received a message: %s", d.Body)
		}
	}()
	log.Logger.Info("Сервис по рассылке email успешно запущен")
	<-forever
}

// Mailing cobra run server
var MailingCmd = &cobra.Command{
	Use:   "email_notify",
	Short: "run email_notify",
	Run: func(cmd *cobra.Command, args []string) {
		n := emailNotify{amqpDsn, amqpQueueName, mailingDBDSN}
		n.RunServerEmailNotify(mailingDBDSN, amqpDsn, amqpQueueName)
	},
}

func init() {
	log.Logger.Info("Init mailing service")
	err := viper.BindEnv("QUEUE_NAME_EMAIL_NOTIFY")
	if err != nil {
		log.Logger.Info(err)
	}
	err = viper.BindEnv("AMQP_DSN")
	if err != nil {
		log.Logger.Info(err)
	}
	err = viper.BindEnv("DB_DSN")
	if err != nil {
		log.Logger.Info(err)
	}
	viper.AutomaticEnv()
	log.Logger.Info(viper.AllSettings())
	amqpDsn = viper.GetString("AMQP_DSN")
	mailingDBDSN = viper.GetString("db_dsn")
	amqpQueueName = viper.GetString("QUEUE_NAME_EMAIL_NOTIFY")
}
