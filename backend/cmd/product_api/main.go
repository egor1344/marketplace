/*
	Запуск rest product_api сервиса
*/

package product_api

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"marketplace/internal/api"
	"marketplace/internal/database/postgres"
	"marketplace/internal/domain/services"
	log "marketplace/pkg/logger"
)

// initRestServer - инициализация rest сервера
func initRestServer() (*api.ProductServer, error) {
	log.Logger.Info("initGrpcServer")
	dbDsn := viper.GetString("DB_DSN")
	if dbDsn == "" {
		log.Logger.Error("dont set env variable DB_DSN")
	}
	database, err := postgres.InitPgBannerStorage(dbDsn)
	if err != nil {
		log.Logger.Error("databases error ", err)
	}
	database.Log, err = log.GetLogger()
	if err != nil {
		log.Logger.Error("get logger error")
	}
	apiService := services.ProductService{Database: database, Log: log.Logger}
	amqpDsn := viper.GetString("AMQP_DSN")
	if amqpDsn == "" {
		log.Logger.Error("dont set env variable AMQP_DSN")
	}
	queueMap := make(map[string]string)
	queueMap["email"] = viper.GetString("QUEUE_NAME_EMAIL_NOTIFY")
	queueMap["push"] = viper.GetString("QUEUE_NAME_PUSH_NOTIFY")
	notify := services.NotifyService{AmqpDSN: amqpDsn, Log: log.Logger, NotifyMap: queueMap}
	return &api.ProductServer{ProductService: &apiService, Log: log.Logger, Notify: notify}, nil
}

// RunProductRestServer - инициализация rest сервера
func RunProductRestServer(c chan bool) {
	log.Logger.Info("run rest product_api server")
	restServer, err := initRestServer()
	if restServer == nil {
		c <- true
		log.Logger.Fatal("Error run server")
		return
	}
	if err != nil {
		log.Logger.Error("error run rest server ", err)
	}
	address := viper.GetString("API_REST_HOST") + ":" + viper.GetString("API_PRODUCT_REST_PORT")
	log.Logger.Info(address)
	restServer.RunServer(address)
	c <- true
}

// RestAPIServerCmd - Комманда для cobra
var RestAPIServerCmd = &cobra.Command{
	Use:   "rest_api_server",
	Short: "run rest product_api server",
	Run: func(cmd *cobra.Command, args []string) {
		c := make(chan bool)
		RunProductRestServer(c)
	},
}

func init() {
	err := viper.BindEnv("DB_DSN")
	err = viper.BindEnv("API_PRODUCT_REST_PORT")
	err = viper.BindEnv("API_PRODUCT_REST_PORT")
	err = viper.BindEnv("AMQP_DSN")
	err = viper.BindEnv("QUEUE_NAME_EMAIL_NOTIFY")
	err = viper.BindEnv("QUEUE_NAME_PUSH_NOTIFY")
	if err != nil {
		log.Logger.Info(err)
	}
	viper.AutomaticEnv()
	log.Logger.Info(viper.AllSettings())
}
