package postgres

/* Пакет для работы с БД Postgres */

import (
	"context"
	_ "github.com/jackc/pgx/stdlib" // stdlib
	"github.com/jmoiron/sqlx"
	"github.com/prometheus/common/log"
	"go.uber.org/zap"
	"marketplace/internal/domain/models"
	"marketplace/internal/domain/models/user"
)

// PgStorage - реализует работу с БД. Реализует интерфейс database
type PgStorage struct {
	DB  *sqlx.DB
	Log *zap.SugaredLogger
}

// InitPgBannerStorage - Инициализация соединения с БД
func InitPgBannerStorage(dsn string) (*PgStorage, error) {
	db, err := sqlx.Open("pgx", dsn)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return &PgStorage{DB: db}, nil
}

// ListProduct - Получение всех списков товара
func (pgbs *PgStorage) ListProduct(ctx context.Context) ([]models.Product, error) {
	pgbs.Log.Info("bd list banner")
	row, err := pgbs.DB.QueryxContext(ctx, `select id, name, price from products;`)
	if err != nil {
		pgbs.Log.Error("error select list product ", err)
		return nil, err
	}
	var listProduct []models.Product
	var prod models.Product
	for row.Next() {
		prod = models.Product{}
		err = row.StructScan(&prod)
		listProduct = append(listProduct, prod)
		pgbs.Log.Info(prod)
	}
	pgbs.Log.Info(listProduct)
	return listProduct, nil
}

// ListProductCart - Получение всех списков товаров в корзине
func (pgbs *PgStorage) ListProductCart(ctx context.Context, idCart int) ([]models.Product, error) {
	pgbs.Log.Info("get product cart ")

	row, err := pgbs.DB.QueryxContext(ctx, `select id, name, price from products where products.id in (select product from users_card_products where users_card=$1);`, idCart)
	if err != nil {
		pgbs.Log.Error("error select list product ", err)
		return nil, err
	}
	var listProduct []models.Product
	var prod models.Product
	for row.Next() {
		prod = models.Product{}
		err = row.StructScan(&prod)
		listProduct = append(listProduct, prod)
		pgbs.Log.Info(prod)
	}
	pgbs.Log.Info(listProduct)
	return listProduct, nil
}

// AddProduct - Добавить товар
func (pgbs *PgStorage) AddProduct(ctx context.Context, name string, price int64) error {
	pgbs.Log.Info("add product")
	_, err := pgbs.DB.ExecContext(ctx, `insert into products(name, price) values($1, $2)`, name, price)
	if err != nil {
		pgbs.Log.Error("error add product ", err)
		return err
	}
	return nil
}

//Удалить товар
func (pgbs *PgStorage) DelProduct(ctx context.Context, idProduct int64) error {
	pgbs.Log.Info("database del product")
	_, err := pgbs.DB.ExecContext(ctx, `delete from products where id=$1;`, idProduct)
	if err != nil {
		pgbs.Log.Error("error del product ", err)
		return err
	}
	return nil
}

//Обновление товара
func (pgbs *PgStorage) UpdateProduct(ctx context.Context, idProduct int64, name string, price int64) (models.Product, error) {
	pgbs.Log.Info("database update product")
	_, err := pgbs.DB.ExecContext(ctx, `update products set name=$1, price=$2 where id=$3;`, name, price, idProduct)
	if err != nil {
		pgbs.Log.Error("error update product ", err)
		return models.Product{}, err
	}
	return models.Product{ID: idProduct, Name: name, Price: int(price)}, nil
}

// Получение информации по одному товару
func (pgbs *PgStorage) GetProduct(ctx context.Context, idProduct int64) (models.Product, error) {
	pgbs.Log.Info("database get product")
	var prod models.Product
	err := pgbs.DB.Get(&prod, "SELECT id, name, price FROM products where id=$1 LIMIT 1;", idProduct)
	if err != nil {
		return models.Product{}, err
	}
	return prod, nil
}

// Найти(загрузить) пользователя
func (pgbs *PgStorage) LoadUser(ctx context.Context, email string) (user.User, error) {
	pgbs.Log.Info("database get user")
	u := user.User{}
	err := pgbs.DB.Get(&u, "select * from users where email=$1 limit 1", email)
	if err != nil {
		pgbs.Log.Error(err)
		return user.User{}, err
	}
	pgbs.Log.Info(u)
	return u, nil
}

// Получить данные пользователя
func (pgbs *PgStorage) GetInfoUser(ctx context.Context, email string) (user.User, error) {
	pgbs.Log.Info("database get user")
	u := user.User{}
	err := pgbs.DB.Get(&u, "select id, fio, email, phone  from users where email=$1 limit 1", email)
	if err != nil {
		pgbs.Log.Error(err)
		return user.User{}, err
	}
	pgbs.Log.Info(u)
	return u, nil
}

// Создать пользователя
func (pgbs *PgStorage) CreateUser(ctx context.Context, newUser user.User) error {
	pgbs.Log.Info("database create user")
	pgbs.Log.Info(ctx, newUser)
	_, err := pgbs.DB.ExecContext(ctx, `insert into users(email, password) values($1, $2)`, newUser.GetPID(), newUser.Password)
	if err != nil {
		pgbs.Log.Error("error create user", err)
		return err
	}
	return nil
}

func (pgbs *PgStorage) CreateCart(ctx context.Context, userID int) error {
	pgbs.Log.Info("database create cart")
	result, err := pgbs.DB.ExecContext(ctx, `insert into users_card(user_id) values($1)`, userID)
	if err != nil {
		pgbs.Log.Error("error create user", err)
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		log.Fatal(err)
		return err
	}
	if rows != 1 {
		log.Fatalf("expected to affect 1 row, affected %d", rows)
	}
	return nil
}

// Получение текущей корзины
func (pgbs *PgStorage) GetActualCart(ctx context.Context, userEmail string) (user.Cart, error) {
	pgbs.Log.Info("database get actual cart")
	loadUser, err := pgbs.GetInfoUser(ctx, userEmail)
	if err != nil {
		pgbs.Log.Error(err)
		return user.Cart{}, err
	}
	cart := user.Cart{}
	err = pgbs.DB.GetContext(ctx, &cart, "select id, date, booked from users_card where user_id=$1 and booked=False limit 1", loadUser.ID)
	if err != nil {
		if err.Error() == `sql: no rows in result set` {
			err = pgbs.CreateCart(ctx, loadUser.ID)
			if err != nil {
				return user.Cart{}, err
			}
			err = pgbs.DB.GetContext(ctx, &cart, "select id, date, booked from users_card where user_id=$1 and booked=False limit 1", loadUser.ID)
			if err != nil {
				pgbs.Log.Error(err)
				return user.Cart{}, err
			}
		} else {
			pgbs.Log.Error(err)
			return user.Cart{}, err

		}
	}
	cart.User = loadUser
	cart.Products, err = pgbs.ListProductCart(ctx, cart.ID)
	if err != nil {
		pgbs.Log.Error("error get cart product", err)
		return user.Cart{}, err
	}
	return cart, nil
}

// Добавление товара в корзину
func (pgbs *PgStorage) AddProductToCart(ctx context.Context, userEmail string, idProduct int64) error {
	pgbs.Log.Info("database get actual cart")
	cart, err := pgbs.GetActualCart(ctx, userEmail)
	if err != nil {
		return err
	}
	result, err := pgbs.DB.ExecContext(ctx, `insert into users_card_products(users_card, product) values($1, $2)`, cart.ID, idProduct)
	if err != nil {
		pgbs.Log.Error("error add product in cart", err)
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		log.Fatal(err)
		return err
	}
	if rows != 1 {
		log.Fatalf("expected to affect 1 row, affected %d", rows)
	}
	return nil
}

// Удаление товара из избранного
func (pgbs *PgStorage) RemoveProductToCart(ctx context.Context, userEmail string, idProduct int64) error {
	pgbs.Log.Info("database remove product cart")
	cart, err := pgbs.GetActualCart(ctx, userEmail)
	if err != nil {
		return err
	}
	result, err := pgbs.DB.ExecContext(ctx, `delete from users_card_products where users_card=$1 and product=$2`, cart.ID, idProduct)
	if err != nil {
		pgbs.Log.Error("error delete product in cart", err)
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		log.Fatal(err)
		return err
	}
	if rows != 1 {
		log.Fatalf("expected to affect 1 row, affected %d", rows)
	}
	return nil
}

// Удаление товара из избранного
func (pgbs *PgStorage) ClearCart(ctx context.Context, userEmail string) error {
	pgbs.Log.Info("database clear cart")
	cart, err := pgbs.GetActualCart(ctx, userEmail)
	if err != nil {
		return err
	}
	result, err := pgbs.DB.ExecContext(ctx, `delete from users_card_products where users_card=$1`, cart.ID)
	if err != nil {
		pgbs.Log.Error("error clear product in cart", err)
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		log.Fatal(err)
		return err
	}
	if rows != 1 {
		log.Fatalf("expected to affect 1 row, affected %d", rows)
	}
	return nil
}

// Получение забронированных корзин
func (pgbs *PgStorage) GetBookedCart(ctx context.Context, userEmail string) ([]user.Cart, error) {
	pgbs.Log.Info("database get booked cart")
	loadUser, err := pgbs.GetInfoUser(ctx, userEmail)
	if err != nil {
		pgbs.Log.Error(err)
		return []user.Cart{}, err
	}
	row, err := pgbs.DB.QueryxContext(ctx, `select id, date, booked from users_card where user_id=$1 and booked=True`, loadUser.ID)
	if err != nil {
		pgbs.Log.Error("error select list product ", err)
		return nil, err
	}
	var listCart []user.Cart
	var cart user.Cart
	for row.Next() {
		cart = user.Cart{}
		err = row.StructScan(&cart)
		cart.User = loadUser
		cart.Products, err = pgbs.ListProductCart(ctx, cart.ID)
		if err != nil {
			pgbs.Log.Error("error get cart product", err)
			return []user.Cart{}, err
		}
		listCart = append(listCart, cart)
		pgbs.Log.Info(cart)
	}
	pgbs.Log.Info(listCart)
	return listCart, nil
}

// Бронирование корзины
func (pgbs *PgStorage) BookedCart(ctx context.Context, userEmail string) error {
	pgbs.Log.Info("database booked cart")
	loadUser, err := pgbs.GetInfoUser(ctx, userEmail)
	if err != nil {
		pgbs.Log.Error(err)
		return err
	}
	result, err := pgbs.DB.ExecContext(ctx, `update users_card set booked=True where user_id=$1 and booked=False`, loadUser.ID)
	if err != nil {
		pgbs.Log.Error("error clear product in cart", err)
		return err
	}
	_, err = result.RowsAffected()
	if err != nil {
		log.Fatal(err)
		return err
	}
	return nil
}

// Close - Закрытие соединения
func (pgbs *PgStorage) Close() {
	err := pgbs.DB.Close()
	if err != nil {
		log.Error(err)
	}
}
