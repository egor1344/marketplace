package api

import (
	"encoding/json"
	"fmt"
	abclientstate "github.com/volatiletech/authboss-clientstate"
	"go.uber.org/zap"
	"log"
	_ "marketplace/docs"
	"marketplace/internal/domain/interfaces"
	"marketplace/internal/domain/models"
	"marketplace/internal/domain/services"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

// CartServer - Реализует работу с rest сервером.
type CartServer struct {
	CartService interfaces.Cart
	Log         *zap.SugaredLogger
	Notify      services.NotifyService
	Session     *abclientstate.SessionStorer
}

// @Summary Получение текущей корзины
// @Description Получение текущей корзины
// @Accept  json
// @Produce  json
// @Success 200 {object} user.Cart
// @Router /cart_api/ [get]
// GetActualCart - Получение текущей корзины
func (cs *CartServer) GetActualCart(w http.ResponseWriter, r *http.Request) {
	cs.Log.Info("rest get actual cart")
	store, err := cs.Session.Get(r, "marketplace")
	uid := fmt.Sprintf("%v", store.Values["uid"])
	cs.Log.Info(uid)
	if uid == "<nil>" {
		w.WriteHeader(401)
		_, err = fmt.Fprint(w, "401 Unauthorized")
		return
	}
	userCart, err := cs.CartService.GetActualCart(r.Context(), uid)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	jsonUserCart, err := json.Marshal(userCart)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	w.WriteHeader(200)
	_, err = fmt.Fprint(w, string(jsonUserCart))
}

// @Summary Добавление товара в корзину
// @Description Добавление товара в корзину
// @Accept  json
// @Produce  json
// @Success 200 {string} string	"{"status": true}"
// @Router /cart_api/add/ [post]
// AddProductToCart - Добавление товара в корзину
func (cs *CartServer) AddProductToCart(w http.ResponseWriter, r *http.Request) {
	cs.Log.Info("rest add product to cart")
	store, err := cs.Session.Get(r, "marketplace")
	uid := fmt.Sprintf("%v", store.Values["uid"])
	cs.Log.Info(uid)
	if uid == "<nil>" {
		w.WriteHeader(401)
		_, err = fmt.Fprint(w, "401 Unauthorized")
		return
	}
	product := &models.Product{}
	err = json.NewDecoder(r.Body).Decode(product)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	err = cs.CartService.AddProductToCart(r.Context(), uid, product.ID)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	w.WriteHeader(200)
	_, err = fmt.Fprint(w, `{"status": true}`)
}

// @Summary Удаление товара из корзины
// @Description Удаление товара из корзины
// @Accept  json
// @Produce  json
// @Success 200 {string} string	"{"status": true}"
// @Router /cart_api/del/ [post]
// RemoveProductToCart - Удаление товара из корзины
func (cs *CartServer) RemoveProductToCart(w http.ResponseWriter, r *http.Request) {
	cs.Log.Info("rest add product to cart")
	store, err := cs.Session.Get(r, "marketplace")
	uid := fmt.Sprintf("%v", store.Values["uid"])
	cs.Log.Info(uid)
	if uid == "<nil>" {
		w.WriteHeader(401)
		_, err = fmt.Fprint(w, "401 Unauthorized")
		return
	}
	product := &models.Product{}
	err = json.NewDecoder(r.Body).Decode(product)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	err = cs.CartService.RemoveProductToCart(r.Context(), uid, product.ID)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	w.WriteHeader(200)
	_, err = fmt.Fprint(w, `{"status": true}`)
}

// @Summary Очистка товаров из корзины
// @Description Очистка товаров из корзины
// @Accept  json
// @Produce  json
// @Success 200 {string} string	"{"status": true}"
// @Router /cart_api/clear/ [post]
// RemoveProductToCart - Очистка товаров из корзины
func (cs *CartServer) ClearCart(w http.ResponseWriter, r *http.Request) {
	cs.Log.Info("rest add product to cart")
	store, err := cs.Session.Get(r, "marketplace")
	uid := fmt.Sprintf("%v", store.Values["uid"])
	cs.Log.Info(uid)
	if uid == "<nil>" {
		w.WriteHeader(401)
		_, err = fmt.Fprint(w, "401 Unauthorized")
		return
	}
	err = cs.CartService.ClearCart(r.Context(), uid)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	w.WriteHeader(200)
	_, err = fmt.Fprint(w, `{"status": true}`)
}

// @Summary Заказ корзины
// @Description Заказ корзины
// @Accept  json
// @Produce  json
// @Success 200 {string} string	"{"status": true}"
// @Router /cart_api/booked/ [get]
// RemoveProductToCart - Заказ корзины
func (cs *CartServer) GetBookedCart(w http.ResponseWriter, r *http.Request) {
	cs.Log.Info("rest booked cart")
	store, err := cs.Session.Get(r, "marketplace")
	uid := fmt.Sprintf("%v", store.Values["uid"])
	cs.Log.Info(uid)
	if uid == "<nil>" {
		w.WriteHeader(401)
		_, err = fmt.Fprint(w, "401 Unauthorized")
		return
	}
	bookedCart, err := cs.CartService.GetBookedCart(r.Context(), uid)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	jsonBookedCart, err := json.Marshal(bookedCart)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	w.WriteHeader(200)
	_, err = fmt.Fprint(w, string(jsonBookedCart))
}

// @Summary Очистка товаров из корзины
// @Description Очистка товаров из корзины
// @Accept  json
// @Produce  json
// @Success 200 {array} user.Cart
// @Router /cart_api/booked/ [post]
// RemoveProductToCart - Очистка товаров из корзины
func (cs *CartServer) BookedCart(w http.ResponseWriter, r *http.Request) {
	cs.Log.Info("rest get booked cart")
	store, err := cs.Session.Get(r, "marketplace")
	uid := fmt.Sprintf("%v", store.Values["uid"])
	cs.Log.Info(uid)
	if uid == "<nil>" {
		w.WriteHeader(401)
		_, err = fmt.Fprint(w, "401 Unauthorized")
		return
	}
	err = cs.CartService.BookedCart(r.Context(), uid)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	w.WriteHeader(200)
	_, err = fmt.Fprint(w, `{"status": true}`)
}

// @title API from markeplace
// @version 0.1.0
// @license.name no-licens
func (cs *CartServer) RunServer(address string) {
	cs.Log.Info("run cart rest server")
	router := mux.NewRouter()
	router.HandleFunc("/cart_api/", cs.GetActualCart).Methods("GET")
	router.HandleFunc("/cart_api/booked/", cs.GetBookedCart).Methods("GET")
	router.HandleFunc("/cart_api/booked/", cs.BookedCart).Methods("POST")
	router.HandleFunc("/cart_api/clear/", cs.ClearCart).Methods("POST")
	router.HandleFunc("/cart_api/add/", cs.AddProductToCart).Methods("POST")
	router.HandleFunc("/cart_api/del/", cs.RemoveProductToCart).Methods("POST")
	// swagger
	srv := &http.Server{
		Handler:      router,
		Addr:         address,
		WriteTimeout: 3 * time.Second,
		ReadTimeout:  3 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
