package api

import (
	"encoding/json"
	"fmt"
	"github.com/swaggo/http-swagger"
	"go.uber.org/zap"
	"log"
	_ "marketplace/docs"
	"marketplace/internal/domain/interfaces"
	"marketplace/internal/domain/models"
	"marketplace/internal/domain/services"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

// ProductServer - Реализует работу с rest сервером.
type ProductServer struct {
	ProductService interfaces.Products
	Log            *zap.SugaredLogger
	Notify         services.NotifyService
}

// @Summary Добавить товар
// @Description добавление товара
// @Accept  json
// @Produce  json
// @Param name body models.Product true "Товар"
// @Success 200 {string} string	"{"status": true}"
// @Router /product_api/products/ [post]
// AddBannerHandler - Добавить товар
func (rbs *ProductServer) AddProductHandler(w http.ResponseWriter, r *http.Request) {
	rbs.Log.Info("rest add product")
	product := &models.Product{}
	err := json.NewDecoder(r.Body).Decode(product)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	rbs.Log.Info(product)
	err = rbs.ProductService.AddProduct(r.Context(), product.Name, int64(product.Price))
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	w.WriteHeader(200)
	_, err = fmt.Fprint(w, `{"status": true}`)
}

// @Summary Получение информации об одном товаре
// @Description Получение информации об одном товаре
// @Accept  json
// @Produce  json
// @Param id path int true "ID товара"
// @Success 200 {object} models.Product
// @Router /product_api/products/{id}/ [get]
// GetProductHandler - Получение информации об одном товаре
func (rbs *ProductServer) GetProductHandler(w http.ResponseWriter, r *http.Request) {
	rbs.Log.Info("rest get one product")
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	prod, err := rbs.ProductService.GetProduct(r.Context(), id)
	w.WriteHeader(200)
	s, err := json.Marshal(prod)
	_, err = fmt.Fprint(w, string(s))
}

// @Summary Удалить товар
// @Description Удалить товар
// @Accept  json
// @Produce  json
// @Param id path int true "ID товара"
// @Success 200 {string} string	"status: true"
// @Router /product_api/products/{id}/ [delete]
// DelProductHandler - Удалить товар
func (rbs *ProductServer) DelProductHandler(w http.ResponseWriter, r *http.Request) {
	rbs.Log.Info("rest del product")
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	err = rbs.ProductService.DelProduct(r.Context(), id)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	w.WriteHeader(200)
	_, err = fmt.Fprint(w, `{"status": true}`)
}

// @Summary Обновление товара
// @Description Обновление товар
// @Accept  json
// @Produce  json
// @Param name body models.Product true "Товар"
// @Success 200 {object} models.Product
// @Router /product_api/products/{id}/ [post]
// UpdateProductHandler - Обновление товара
func (rbs *ProductServer) UpdateProductHandler(w http.ResponseWriter, r *http.Request) {
	rbs.Log.Info("rest update product ")
	vars := mux.Vars(r)
	id, err := strconv.ParseInt(vars["id"], 10, 64)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	product := &models.Product{}
	err = json.NewDecoder(r.Body).Decode(product)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	rbs.Log.Info(product)
	*product, err = rbs.ProductService.UpdateProduct(r.Context(), id, product.Name, int64(product.Price))
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	w.WriteHeader(200)
	s, err := json.Marshal(product)
	_, err = fmt.Fprint(w, string(s))
}

// @Summary Получение списка  товаров
// @Description Получение списка  товаров
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Product
// @Router /product_api/products/ [get]
// ListProductHandler - Получение списка  товаров
func (rbs *ProductServer) ListProductHandler(w http.ResponseWriter, r *http.Request) {
	rbs.Log.Info("rest list product ")
	listProduct, err := rbs.ProductService.ListProduct(r.Context())
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	jsonProduct, err := json.Marshal(listProduct)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	_, err = fmt.Fprint(w, string(jsonProduct))
}

// @title API from markeplace
// @version 0.1.0
// @license.name no-licens
func (rbs *ProductServer) RunServer(address string) {
	rbs.Log.Info("run rest server")
	router := mux.NewRouter()
	router.HandleFunc("/product_api/products/", rbs.ListProductHandler).Methods("GET")
	router.HandleFunc("/product_api/products/", rbs.AddProductHandler).Methods("POST")
	router.HandleFunc("/product_api/products/{id}/", rbs.DelProductHandler).Methods("DELETE")
	router.HandleFunc("/product_api/products/{id}/", rbs.GetProductHandler).Methods("GET")
	router.HandleFunc("/product_api/products/{id}/", rbs.UpdateProductHandler).Methods("POST")
	// swagger
	router.PathPrefix("/doc/").Handler(httpSwagger.WrapHandler)
	srv := &http.Server{
		Handler:      router,
		Addr:         address,
		WriteTimeout: 3 * time.Second,
		ReadTimeout:  3 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
