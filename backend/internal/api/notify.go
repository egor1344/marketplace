package api

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger"
	"go.uber.org/zap"
	"log"
	"marketplace/internal/domain/models"
	"marketplace/internal/domain/services"
	"net/http"
	"time"
)

// NotifyServer - Реализует работу с rest сервером.
type NotifyServer struct {
	Log    *zap.SugaredLogger
	Notify services.NotifyService
}

// @Summary Уведомить пользователя
// @Description Уведомить пользователя type_notify - (email, push)
// @Accept  json
// @Produce  json
// @Param name body models.Notify true "Уведомление"
// @Success 200 {string} string	"{"status": true}"
// @Router /notify_api/ [post]
// NotifyHandler -Уведомить пользователя
func (ns *NotifyServer) NotifyHandler(w http.ResponseWriter, r *http.Request) {
	ns.Log.Info("rest notify")
	notify := &models.Notify{}
	err := json.NewDecoder(r.Body).Decode(notify)
	if err != nil {
		w.WriteHeader(500)
		_, err = fmt.Fprint(w, "Error ", err)
		return
	}
	ns.Log.Info(notify)
	ns.Notify.SendNotifyInQueue(notify.TypeNotify, string(notify.UserID))
	w.WriteHeader(200)
	_, err = fmt.Fprint(w, `{"status": true}`)
}

// @title API from markeplace
// @version 0.1.0
// @license.name no-licens
func (ns *NotifyServer) RunServer(address string) {
	ns.Log.Info("run rest server")
	router := mux.NewRouter()
	router.HandleFunc("/notify_api/", ns.NotifyHandler).Methods("POST")
	// swagger
	router.PathPrefix("/doc/").Handler(httpSwagger.WrapHandler)
	srv := &http.Server{
		Handler:      router,
		Addr:         address,
		WriteTimeout: 3 * time.Second,
		ReadTimeout:  3 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
