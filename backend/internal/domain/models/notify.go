package models

// Notify - модель обьекта товара
type Notify struct {
	TypeNotify string `json:"type_notify"`
	UserID     int    `db:"user_id" json:"user_id"`
}
