package user

import "marketplace/internal/domain/models"

// CartProduct - модель обьекта корзины пользователя
type CartProduct struct {
	UserCard          Cart
	Product           models.Product
	NotifyCardProduct bool
}
