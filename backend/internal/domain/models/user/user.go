package user

// User - модель обьекта пользователя
type User struct {
	ID  int    `db:"id" json:"id"`
	FIO string `db:"fio" json:"fio"`

	// Auth
	Email    string `db:"email" json:"email"`
	Password string `db:"password"`

	Phone string `db:"phone" json:"phone"`
}

// PutPID into user
func (u *User) PutPID(pid string) { u.Email = pid }

// GetPID from user
func (u User) GetPID() string { return u.Email }

// PutPassword into user
func (u *User) PutPassword(password string) { u.Password = password }

// GetPassword from user
func (u User) GetPassword() string { return u.Password }
