package user

import (
	"marketplace/internal/domain/models"
	"time"
)

// Cart - модель обьекта корзины пользователя
type Cart struct {
	ID       int       `db:"id" json:"id"`
	Date     time.Time `db:"date" json:"date"`
	Booked   bool      `db:"booked" json:"booked"`
	User     User
	Products []models.Product
}
