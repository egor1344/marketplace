package models

// Product - модель обьекта товара
type Product struct {
	ID    int64  `db:"id" json:"id"`
	Price int    `db:"price" json:"price"`
	Name  string `db:"name" json:"name"`
}
