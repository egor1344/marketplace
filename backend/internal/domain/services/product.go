package services

import (
	"context"
	"go.uber.org/zap"
	"marketplace/internal/domain/interfaces/databases"
	"marketplace/internal/domain/models"
)

type ProductService struct {
	Database databases.ProductDatabase // Интерфейс для работы с БД
	Log      *zap.SugaredLogger        // Логгер
}

func (ps *ProductService) ListProduct(ctx context.Context) ([]models.Product, error) {
	ps.Log.Info("ps service list_product")
	return ps.Database.ListProduct(ctx)
}

// Удалить товар
func (ps *ProductService) DelProduct(ctx context.Context, idProduct int64) error {
	ps.Log.Info("DelProduct")
	return ps.Database.DelProduct(ctx, idProduct)

}

// Обновление товара
func (ps *ProductService) UpdateProduct(ctx context.Context, idProduct int64, name string, price int64) (models.Product, error) {
	ps.Log.Info("UpdateProduct")
	return ps.Database.UpdateProduct(ctx, idProduct, name, price)
}

// Получение информации по одному товару
func (ps *ProductService) GetProduct(ctx context.Context, idProduct int64) (models.Product, error) {
	ps.Log.Info("GetProduct")
	return ps.Database.GetProduct(ctx, idProduct)
}

// AddProduct
func (ps *ProductService) AddProduct(ctx context.Context, name string, price int64) error {
	ps.Log.Info("add product")
	return ps.Database.AddProduct(ctx, name, price)
}
