package services

import (
	"context"
	"go.uber.org/zap"
	"marketplace/internal/domain/interfaces/databases"
	"marketplace/internal/domain/models/user"
)

type CartService struct {
	Database databases.CartDatabase // Интерфейс для работы с БД
	Log      *zap.SugaredLogger     // Логгер
}

// Добавить товар в корзину
func (cs *CartService) AddProductToCart(ctx context.Context, userEmail string, idProduct int64) error {
	cs.Log.Info("cs service AddProductToCart")
	return cs.Database.AddProductToCart(ctx, userEmail, idProduct)
}

// Удалить товар из корзины
func (cs *CartService) RemoveProductToCart(ctx context.Context, userEmail string, idProduct int64) error {
	cs.Log.Info("cs service RemoveProductToCart")
	return cs.Database.RemoveProductToCart(ctx, userEmail, idProduct)
}

// Очистить корзину
func (cs *CartService) ClearCart(ctx context.Context, userEmail string) error {
	cs.Log.Info("cs service ClearCart")
	return cs.Database.ClearCart(ctx, userEmail)
}

// Сделать заказ корзины
func (cs *CartService) BookedCart(ctx context.Context, userEmail string) error {
	cs.Log.Info("cs service BookedCart")
	return cs.Database.BookedCart(ctx, userEmail)
}

// Получение текущей корзины
func (cs *CartService) GetActualCart(ctx context.Context, userEmail string) (user.Cart, error) {
	cs.Log.Info("cs service GetActualCart")
	return cs.Database.GetActualCart(ctx, userEmail)
}

// Получение забронированых корзин
func (cs *CartService) GetBookedCart(ctx context.Context, userEmail string) ([]user.Cart, error) {
	cs.Log.Info("cs service GetBookedCart")
	return cs.Database.GetBookedCart(ctx, userEmail)
}
