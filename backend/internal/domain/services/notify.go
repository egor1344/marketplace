package services

import (
	"github.com/streadway/amqp"
	"go.uber.org/zap"
)

type NotifyService struct {
	Log       *zap.SugaredLogger // Логгер
	AmqpDSN   string             // Доступ к rabbitMQ
	NotifyMap map[string]string  // мапа где ключ - тип уведомелния, значение -название очереди в rabbitmq
	rmqCh     *amqp.Channel      // открытый канал
}

// InitConnection - подключение к RabbitMQ
func (rbs *NotifyService) SendNotifyInQueue(amqpQueueName, body string) {

	rbs.Log.Info(amqpQueueName, body)
	// RabbitMQ
	conn, err := amqp.Dial(rbs.AmqpDSN)
	if err != nil {
		rbs.Log.Fatal(err)
	}
	defer conn.Close()
	rmqCh, err := conn.Channel()
	if err != nil {
		rbs.Log.Fatal(err)
	}
	defer rmqCh.Close()
	queueName, ok := rbs.NotifyMap[amqpQueueName]
	if !ok { //  если такого метода нет
		return
	}
	rbs.pullInQueue(body, rmqCh, queueName)
}

// pullInQueue Отправка сообщений в очередь
func (rbs *NotifyService) pullInQueue(body string, rmqCh *amqp.Channel, queueName string) {
	q, err := rmqCh.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when unused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		rbs.Log.Fatal(err)
	}
	err = rmqCh.Publish(
		"",
		q.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(body),
		},
	)
	if err != nil {
		rbs.Log.Fatal(err)
	}
}
