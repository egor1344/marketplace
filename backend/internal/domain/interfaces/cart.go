package interfaces

import (
	"context"
	"marketplace/internal/domain/models/user"
)

// Cart - Интерфейс реализирующий "прослойку" между API(в данном случае GRPC, REST API) и БД
type Cart interface {
	// Добавить товар в корзину
	AddProductToCart(ctx context.Context, userEmail string, idProduct int64) error
	// Удалить товар из корзины
	RemoveProductToCart(ctx context.Context, userEmail string, idProduct int64) error
	// Очистить корзину
	ClearCart(ctx context.Context, userEmail string) error
	// Сделать заказ корзины
	BookedCart(ctx context.Context, userEmail string) error
	// Получение текущей корзины
	GetActualCart(ctx context.Context, userEmail string) (user.Cart, error)
	// Получение забронированых корзин
	GetBookedCart(ctx context.Context, userEmail string) ([]user.Cart, error)
}
