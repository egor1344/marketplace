package interfaces

import (
	"context"
)

// Notify - Интерфейс утверждающий методы для работы с уведомлениями от различных источников
type Notify interface {
	// SendUserChangePriceInCard - уведомить пользователя о изменеии цены товара в его корзине
	SendUserChangePriceInCard(ctx context.Context, userID int) error
}
