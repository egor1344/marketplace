package databases

import (
	"context"
	"marketplace/internal/domain/models/user"
)

// AuthDatabase - Интерфейс реализирующий работу с БД для авторизации пользователей
type AuthDatabase interface {
	// Найти(загрузить) пользователя
	LoadUser(ctx context.Context, email string) (user.User, error)
	// Создать пользователя
	CreateUser(ctx context.Context, newUser user.User) error
}
