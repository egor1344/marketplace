package databases

import (
	"context"
	"marketplace/internal/domain/models"
)

// ProductDatabase - Интерфейс реализирующий работу с БД
type ProductDatabase interface {
	// Добавить товар
	AddProduct(ctx context.Context, name string, price int64) error
	//Удалить товар
	DelProduct(ctx context.Context, idProduct int64) error
	//Обновление товара
	UpdateProduct(ctx context.Context, idProduct int64, name string, price int64) (models.Product, error)
	// Получение списка товаров
	ListProduct(ctx context.Context) ([]models.Product, error)
	// Получение информации по одному товару
	GetProduct(ctx context.Context, idProduct int64) (models.Product, error)
}
