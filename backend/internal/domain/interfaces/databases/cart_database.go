package databases

import (
	"context"
	"marketplace/internal/domain/models/user"
)

// CartDatabase - Интерфейс реализирующий работу с БД для работы с корзиной
type CartDatabase interface {
	// Добавить товар в корзину
	AddProductToCart(ctx context.Context, userEmail string, idProduct int64) error
	// Удалить товар из корзины
	RemoveProductToCart(ctx context.Context, userEmail string, idProduct int64) error
	// Очистить корзину
	ClearCart(ctx context.Context, userEmail string) error
	// Сделать заказ корзины
	BookedCart(ctx context.Context, userEmail string) error
	// Получение текущей корзины
	GetActualCart(ctx context.Context, userEmail string) (user.Cart, error)
	// Получение забронированых корзин
	GetBookedCart(ctx context.Context, userEmail string) ([]user.Cart, error)
}
